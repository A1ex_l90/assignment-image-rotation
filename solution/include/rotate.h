#pragma once

#include "image.h"
#include <stdlib.h>
#include <string.h>

struct image rotate(struct image* const source) {

    struct image image;

    image.height = source->width;
    image.width = source->height;
    image.data = malloc(3 * image.height * image.width);

    for (int i = 0; i < source->height; i++) {
        for (int j = 0; j < source->width; j++) {
            image.data[j * source->height + image.width - i - 1] = source->data[i * source->width + j];
        }
    }
    return image;
}
