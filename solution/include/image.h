#pragma once

#include  <stdint.h>

#pragma pack(push, 1)

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

#pragma pack(pop)
