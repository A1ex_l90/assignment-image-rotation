
#include "bmp_read_write.h"
#include "rotate.h"

#include <stdio.h>

#define RB "rb"
#define WB "wb"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning


    if (argc != 3) {
        printf("%s", "WRONG_NUMBER_OF_ARGUMENTS");
    } else {

        char* source_image = argv[1];
        char* transformed_image = argv[2];

        FILE* from_bmp_in;
        FILE* from_bmp_out;

        if ((from_bmp_in = fopen(source_image, RB)) == NULL) {
            printf("%s", "CAN_NOT_OPEN_FILE");
            return 1;
        }

        if ((from_bmp_out = fopen(transformed_image, WB)) == NULL) {
            printf("%s", "CAN_NOT_OPEN_FILE");
            return 1;
        }

        struct image image_bmp;

        enum read_status read_status =  from_bmp(from_bmp_in, &image_bmp);

        if (read_status == READ_INVALID_SIGNATURE) {
            printf("%s", "READ_INVALID_SIGNATURE");
        } else if (read_status == READ_INVALID_BITS) {
            printf("%s", "READ_INVALID_BITS");
        } else if (read_status == READ_INVALID_HEADER) {
            printf("%s", "READ_INVALID_HEADER");
        } else if (read_status == READ_OK) {
            struct image rotate_image = rotate(&image_bmp);
            enum write_status write_status =  to_bmp(from_bmp_out, &rotate_image);
            if (write_status == WRITE_OK) {
                printf("%s", "WRITE_OK");
            } else {
                printf("%s", "WRITE_ERROR");
            }
            free(rotate_image.data);
        } else {
            printf("%s", "UNKNOWN_READ_STATUS");
        }

        free(image_bmp.data);
        fclose(from_bmp_in);
        fclose(from_bmp_out);

    }

    return 0;
}
