#include "bmp_read_write.h"

#define bfTypeSTANDART 19778
#define bfReservedSTANDART 0
#define bOffBitsSTANDART 54
#define biSizeSTANDART 40
#define biPlanesSTANDART 1
#define biBitCountSTANDART 24
#define biCompressionSTANDART 0
#define biSizeImageSTANDART 0
#define biXPelsPerMeterSTANDART 0
#define biYPelsPerMeterSTANDART 0
#define biClrUsedSTANDART 0
#define biClrImportantSTANDART 0

size_t calculate_padding(size_t wight) {
    return (4 - (wight * 3) % 4) % 4;
}

enum read_status from_bmp(FILE *in, struct image *img) {

    struct bmp_header header;

    if (fread(&header, bOffBitsSTANDART, 1, in) != 1) {
        return READ_INVALID_BITS;
    }

    if (header.bfType != bfTypeSTANDART && header.bfType) {
        return READ_INVALID_SIGNATURE;
    }

    if (header.biSize != biSizeSTANDART) {
        return READ_INVALID_HEADER;
    }

    if (header.biBitCount != biBitCountSTANDART) {
        return READ_INVALID_BITS;
    }

    img->height = header.biHeight;
    img->width = header.biWidth;

    if ((img->data = malloc(sizeof(struct pixel) * header.biHeight * header.biWidth)) == NULL) {
        return MALLOC_ERROR;
    }

    char tmp[4] = "\0\0\0\0";

    for (int i = 0; i < img->height; i++) {
        for (int j = 0; j < img->width; j++) {
            if (fread(&(img->data[i * img->width + j]), sizeof(struct pixel), 1, in) != 1) {
                return READ_INVALID_BITS;
            }
        }
        if (fread(&tmp, 1, calculate_padding(img->width), in) != calculate_padding(img->width)) {
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}

struct bmp_header create_header(size_t width, size_t height) {

    struct bmp_header header;

    header.bfType = bfTypeSTANDART;
    header.bfileSize = bOffBitsSTANDART + (width * 3 + calculate_padding(width) * height);
    header.bfReserved = bfReservedSTANDART;
    header.bOffBits = bOffBitsSTANDART;
    header.biSize = biSizeSTANDART;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = biPlanesSTANDART;
    header.biBitCount = biBitCountSTANDART;
    header.biCompression = biCompressionSTANDART;
    header.biSizeImage = biSizeImageSTANDART;
    header.biXPelsPerMeter = biXPelsPerMeterSTANDART;
    header.biYPelsPerMeter = biYPelsPerMeterSTANDART;
    header.biClrUsed = biClrUsedSTANDART;
    header.biClrImportant = biClrImportantSTANDART;

    return header;
}

enum write_status to_bmp(FILE *out, struct image const *img) {

    struct bmp_header header = create_header(img->width, img->height);

    if (fwrite(&header, bOffBitsSTANDART, 1, out) != 1) {
        return WRITE_ERROR;
    }

    char tmp[4] = "\0\0\0\0";

    for (int i = 0; i < img->height; i++) {

        for (int j = 0; j < img->width; j++) {
            if (fwrite(&(img->data[i * img->width + j]), 3, 1, out) != 1) {
                return WRITE_ERROR;
            }
        }
        if (fwrite(&tmp, 1, calculate_padding(img->width), out) != calculate_padding(img->width)) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
